package com.antoniov.flipppeople.ui

import com.antoniov.flipppeople.models.Cart
import com.antoniov.flipppeople.ui.list.ListViewModel
import com.antoniov.flipppeople.ui.list.interfaces.IUseCaseList

object injection {

    fun provideCart(): Cart = Cart

    fun provideCartViewModel(iUseCaseList: IUseCaseList.View): IUseCaseList =
        ListViewModel(provideCart(), iUseCaseList)
}