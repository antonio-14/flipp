package com.antoniov.flipppeople.ui.splashscreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.antoniov.flipppeople.MainActivity
import com.antoniov.flipppeople.R
import com.antoniov.flipppeople.utils.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash_screen.*

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        executeThread()
    }

    private fun executeThread(){
        Thread{
            run {
                runOnUiThread {
                    AnimationUtils.doBounceAnimation(this, logo)
                }

                Thread.sleep(5000)

                val intent = Intent(
                    this,
                    MainActivity::class.java
                )
                startActivity(intent)

                finish()
            }
        }.start()
    }
}
