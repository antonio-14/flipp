package com.antoniov.flipppeople.ui.browse

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.antoniov.flipppeople.ui.browse.interfaces.IUseCaseStores
import com.antoniov.flipppeople.utils.vo.Resource
import kotlinx.coroutines.Dispatchers

class BrowseViewModel(iUseCaseStores: IUseCaseStores) : ViewModel() {

    val fetchStores = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(iUseCaseStores.getStores())
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }
}