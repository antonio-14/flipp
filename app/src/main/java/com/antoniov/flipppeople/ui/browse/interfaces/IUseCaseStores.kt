package com.antoniov.flipppeople.ui.browse.interfaces


import com.antoniov.flipppeople.models.Stores
import com.antoniov.flipppeople.utils.vo.Resource

interface IUseCaseStores {
    suspend fun getStores(): Resource<MutableList<Stores>>
}