package com.antoniov.flipppeople.ui.detailsWalmart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.antoniov.flipppeople.models.Cart
import com.antoniov.flipppeople.models.Items
import com.antoniov.flipppeople.ui.detailsWalmart.interfaces.IUseCaseItems
import com.antoniov.flipppeople.utils.vo.Resource
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class DetailsWalmartViewModel(iUseCaseItems: IUseCaseItems): ViewModel() {

    val fetchItems = liveData(Dispatchers.IO) {
        try {
            emit(iUseCaseItems.getItems())
        } catch (e:Exception) {
            emit(Resource.Failure(e))
        }
    }

    fun removeItem(item: Items) {
        Cart.removeItem(item)
    }

    fun addItem(item: Items) {
        Cart.addItem(item)
    }
}