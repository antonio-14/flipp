package com.antoniov.flipppeople.ui.browse

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.antoniov.flipppeople.R
import com.antoniov.flipppeople.ui.adapters.StoresAdapter
import com.antoniov.flipppeople.utils.loadingUtils.LoadingUtils
import com.antoniov.flipppeople.utils.vo.Resource
import kotlinx.android.synthetic.main.fragment_browse.*

class BrowseFragment : Fragment() {

    private val viewModel by lazy { ViewModelProvider(this, BrowseViewModelFactory(
        UseCaseStores(
            BrowseRepository()
        )
    )
    ).get(BrowseViewModel::class.java) }


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_browse, container, false)

        observeData()

        return root
    }

    private fun observeData() {
        viewModel.fetchStores.observe(viewLifecycleOwner, Observer {result ->
            when(result) {
                is Resource.Loading -> { LoadingUtils.showLottie(animation, layoutLotie) }
                is Resource.Success -> {
                    val stores = result.data
                    stores.let { stores ->
                        if (rvStores.layoutManager == null) {
                            rvStores.layoutManager = LinearLayoutManager(activity?.applicationContext)
                        }
                        (rvStores?.adapter as StoresAdapter?)?.run {
                            notifyDataSetChanged()
                        } ?: run {
                            rvStores?.adapter = parentFragment?.let { StoresAdapter(stores, it) }
                            LoadingUtils.hideLottie(animation, layoutLotie)
                        }
                    }
                }
                is Resource.Failure -> {
                    Log.e("ERROR", "${result.exception.message}")
                }
            }
        })
    }

    companion object {
        const val TAG_LOGS = "movies"
    }
}
