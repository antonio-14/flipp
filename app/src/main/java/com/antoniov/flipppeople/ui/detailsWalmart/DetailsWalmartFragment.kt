package com.antoniov.flipppeople.ui.detailsWalmart

import android.animation.ValueAnimator
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.airbnb.lottie.LottieAnimationView

import com.antoniov.flipppeople.R
import com.antoniov.flipppeople.ui.adapters.ItemsAdapter
import com.antoniov.flipppeople.utils.vo.Resource
import kotlinx.android.synthetic.main.fragment_details_walmart.*
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.view_list.*
import kotlin.math.abs

class DetailsWalmartFragment : Fragment() {

    private val viewModel by lazy { ViewModelProvider(this, DetailsWalmartViewModelFactory(
        UseCaseItems(DetailsWalmartRepository)
    )
    ).get(DetailsWalmartViewModel::class.java) }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_details_walmart, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ClickItemOne()
        ClickItemTwo()
        ClickItemThree()
        ClickItemFour()
        ClickItemFive()
        ClickItemSix()
        ClickItemSeven()
        ClickItemEight()
    }



    private fun ClickItemOne() {
        layoutItemOne.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item1 = items[14]
                        if (!item1.isSelect) {
                            selectOne.playAnimation()
                            viewModel.addItem(item1)
                        } else {
                            playReverseSelectAnimation(selectOne)
                            viewModel.removeItem(item1)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemTwo() {
        layoutItemTwo.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item2 = items[12]
                        if (!item2.isSelect) {
                            selectTwo.playAnimation()
                            viewModel.addItem(item2)
                        } else {
                            playReverseSelectAnimation(selectTwo)
                            viewModel.removeItem(item2)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemThree() {
        layoutItemThree.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item3 = items[8]
                        if (!item3.isSelect) {
                            selectThree.playAnimation()
                            viewModel.addItem(item3)
                        } else {
                            playReverseSelectAnimation(selectThree)
                            viewModel.removeItem(item3)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemFour() {
        layoutItemFour.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item4 = items[17]
                        if (!item4.isSelect) {
                            selectFour.playAnimation()
                            viewModel.addItem(item4)
                        } else {
                            playReverseSelectAnimation(selectFour)
                            viewModel.removeItem(item4)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemFive() {
        layoutItemFive.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item5 = items[9]
                        if (!item5.isSelect) {
                            selectFive.playAnimation()
                            viewModel.addItem(item5)
                        } else {
                            playReverseSelectAnimation(selectFive)
                            viewModel.removeItem(item5)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemSix() {
        layoutItemSix.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item6 = items[3]
                        if (!item6.isSelect) {
                            selectSix.playAnimation()
                            viewModel.addItem(item6)
                        } else {
                            playReverseSelectAnimation(selectSix)
                            viewModel.removeItem(item6)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemSeven() {
        layoutItemSeven.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item7 = items[15]
                        if (!item7.isSelect) {
                            selectSeven.playAnimation()
                            viewModel.addItem(item7)
                        } else {
                            playReverseSelectAnimation(selectSeven)
                            viewModel.removeItem(item7)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemEight() {
        layoutItemEight.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item8 = items[2]
                        if (!item8.isSelect) {
                            selectEight.playAnimation()
                            viewModel.addItem(item8)
                        } else {
                            playReverseSelectAnimation(selectEight)
                            viewModel.removeItem(item8)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun playReverseSelectAnimation(animationView: LottieAnimationView) {
        val progress = 0.5f
        val valueAnimator = ValueAnimator.ofFloat(-progress, 0f).setDuration((animationView.duration * progress).toLong())
        valueAnimator.addUpdateListener { animation -> animationView.progress = abs(animation.animatedValue as Float) }
        valueAnimator.start()
    }


}
