package com.antoniov.flipppeople.ui.detailsWalgreens

import android.animation.ValueAnimator
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.airbnb.lottie.LottieAnimationView

import com.antoniov.flipppeople.R
import com.antoniov.flipppeople.ui.detailsWalmart.DetailsWalmartRepository
import com.antoniov.flipppeople.ui.detailsWalmart.DetailsWalmartViewModel
import com.antoniov.flipppeople.ui.detailsWalmart.DetailsWalmartViewModelFactory
import com.antoniov.flipppeople.ui.detailsWalmart.UseCaseItems
import com.antoniov.flipppeople.utils.vo.Resource
import kotlinx.android.synthetic.main.fragment_walgreens.*
import kotlin.math.abs

class WalgreensFragment : Fragment() {

    private val viewModel by lazy { ViewModelProvider(this, DetailsWalmartViewModelFactory(
        UseCaseItems(DetailsWalmartRepository)
    )
    ).get(DetailsWalmartViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_walgreens, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ClickItemNine()
        ClickItemTen()
        ClickItemEleven()
        ClickItemTwelve()
        ClickItemThirdteen()
        ClickItemFourteen()
        ClickItemFiveteen()
        ClickItemSixteen()
        ClickItemSeventeen()
        ClickItemEighteen()
    }

    private fun ClickItemNine() {
        layoutItemNine.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item9 = items[10]
                        if (!item9.isSelect) {
                            selectNine.playAnimation()
                            viewModel.addItem(item9)
                        } else {
                            playReverseSelectAnimation(selectNine)
                            viewModel.removeItem(item9)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemTen() {
        layoutItemTen.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item10 = items[0]
                        if (!item10.isSelect) {
                            selectTen.playAnimation()
                            viewModel.addItem(item10)
                        } else {
                            playReverseSelectAnimation(selectTen)
                            viewModel.removeItem(item10)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemEleven() {
        layoutItemEleven.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item11 = items[13]
                        if (!item11.isSelect) {
                            selectEleven.playAnimation()
                            viewModel.addItem(item11)
                        } else {
                            playReverseSelectAnimation(selectEleven)
                            viewModel.removeItem(item11)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemTwelve() {
        layoutItemTwelve.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item10 = items[1]
                        if (!item10.isSelect) {
                            selectTen.playAnimation()
                            viewModel.addItem(item10)
                        } else {
                            playReverseSelectAnimation(selectTen)
                            viewModel.removeItem(item10)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemThirdteen() {
        layoutThirdTeen.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item13 = items[4]
                        if (!item13.isSelect) {
                            selectThirdteen.playAnimation()
                            viewModel.addItem(item13)
                        } else {
                            playReverseSelectAnimation(selectThirdteen)
                            viewModel.removeItem(item13)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemFourteen() {
        layoutItemTen.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item10 = items[0]
                        if (!item10.isSelect) {
                            selectTen.playAnimation()
                            viewModel.addItem(item10)
                        } else {
                            playReverseSelectAnimation(selectTen)
                            viewModel.removeItem(item10)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemFiveteen() {
        layoutItemFiveteen.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item15 = items[11]
                        if (!item15.isSelect) {
                            selectFiveteen.playAnimation()
                            viewModel.addItem(item15)
                        } else {
                            playReverseSelectAnimation(selectFiveteen)
                            viewModel.removeItem(item15)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemSeventeen() {
        layoutItemSeventeen.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item17 = items[16]
                        if (!item17.isSelect) {
                            selectSeventeen.playAnimation()
                            viewModel.addItem(item17)
                        } else {
                            playReverseSelectAnimation(selectSeventeen)
                            viewModel.removeItem(item17)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemSixteen() {
        layoutItemSixteen.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item16 = items[5]
                        if (!item16.isSelect) {
                            selectSixteen.playAnimation()
                            viewModel.addItem(item16)
                        } else {
                            playReverseSelectAnimation(selectSixteen)
                            viewModel.removeItem(item16)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }

    private fun ClickItemEighteen() {
        layoutItemEighteen.setOnClickListener{
            viewModel.fetchItems.observe(viewLifecycleOwner, Observer {result->
                when(result) {
                    is Resource.Success -> {
                        val items = result.data
                        val item18 = items[7]
                        if (!item18.isSelect) {
                            selectEighteen.playAnimation()
                            viewModel.addItem(item18)
                        } else {
                            playReverseSelectAnimation(selectEighteen)
                            viewModel.removeItem(item18)
                        }
                    }
                    is Resource.Failure -> {
                        Log.e("ERROR", "${result.exception.message}")
                    }
                }
            })
        }
    }


    private fun playReverseSelectAnimation(animationView: LottieAnimationView) {
        val progress = 0.5f
        val valueAnimator = ValueAnimator.ofFloat(-progress, 0f).setDuration((animationView.duration * progress).toLong())
        valueAnimator.addUpdateListener { animation -> animationView.progress = abs(animation.animatedValue as Float) }
        valueAnimator.start()
    }

}
