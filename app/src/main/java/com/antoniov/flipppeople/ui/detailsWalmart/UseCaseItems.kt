package com.antoniov.flipppeople.ui.detailsWalmart

import com.antoniov.flipppeople.models.Items
import com.antoniov.flipppeople.ui.detailsWalmart.interfaces.IItemsRepository
import com.antoniov.flipppeople.ui.detailsWalmart.interfaces.IUseCaseItems
import com.antoniov.flipppeople.utils.vo.Resource

class UseCaseItems(private val iRepoItems: IItemsRepository): IUseCaseItems {
    override suspend fun getItems(): Resource<MutableList<Items>> = iRepoItems.getItemsRepo()
}