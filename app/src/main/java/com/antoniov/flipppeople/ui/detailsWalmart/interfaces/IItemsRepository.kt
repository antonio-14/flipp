package com.antoniov.flipppeople.ui.detailsWalmart.interfaces

import com.antoniov.flipppeople.models.Items
import com.antoniov.flipppeople.utils.vo.Resource

interface IItemsRepository {
    suspend fun getItemsRepo(): Resource<MutableList<Items>>
}