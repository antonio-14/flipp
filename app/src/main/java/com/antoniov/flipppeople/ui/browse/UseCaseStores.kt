package com.antoniov.flipppeople.ui.browse

import com.antoniov.flipppeople.models.Stores
import com.antoniov.flipppeople.ui.browse.interfaces.IStoresRepository
import com.antoniov.flipppeople.ui.browse.interfaces.IUseCaseStores
import com.antoniov.flipppeople.utils.vo.Resource

class UseCaseStores(private val iRepo: IStoresRepository): IUseCaseStores {
    override suspend fun getStores(): Resource<MutableList<Stores>> = iRepo.getStoresRepo()
}