package com.antoniov.flipppeople.ui.browse.interfaces

import com.antoniov.flipppeople.models.Stores
import com.antoniov.flipppeople.utils.vo.Resource

interface IStoresRepository {
   suspend fun getStoresRepo(): Resource<MutableList<Stores>>
}