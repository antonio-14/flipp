package com.antoniov.flipppeople.ui.browse

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.antoniov.flipppeople.ui.browse.interfaces.IUseCaseStores

class BrowseViewModelFactory(private val iUseCaseStores: IUseCaseStores): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
       return modelClass.getConstructor(IUseCaseStores::class.java).newInstance(iUseCaseStores)
    }
}