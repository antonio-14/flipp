package com.antoniov.flipppeople.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.antoniov.flipppeople.R
import com.antoniov.flipppeople.models.Stores
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.item_stores.view.*


class StoresAdapter(private val listStores: MutableList<Stores>,  private val fragment: Fragment) :
    RecyclerView.Adapter<StoresAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context), parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with (listStores[position]){

            val context = holder.image.context
            holder.title.text = title
            holder.description.text = description

            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(16))

            Glide.with(context)
                .load(photo)
                .apply(requestOptions)
                .error(R.drawable.image_placeholder)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.image)

            Glide.with(context)
                .load(icon)
                .error(R.drawable.image_placeholder)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.smallIcon)

            when (id) {
                1 -> {
                    holder.itemView.setOnClickListener {
                        fragment.findNavController().navigate(R.id.navigation_detail_store)
                    }
                }
                2 -> {
                    holder.itemView.setOnClickListener {
                        fragment.findNavController().navigate(R.id.navigation_detail_familydollar)
                    }
                }
                3 -> {
                    holder.itemView.setOnClickListener {
                        fragment.findNavController().navigate(R.id.navigation_detail_walgreens)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return listStores.size
    }

    inner class ViewHolder internal constructor(
        inflater: LayoutInflater,
        parent: ViewGroup
    ) : RecyclerView.ViewHolder(
        inflater.inflate(R.layout.item_stores, parent, false)
    ) {
        internal val title: TextView = itemView.tvTitle
        internal val description: TextView = itemView.tvDescripcion
        internal val image: ImageView = itemView.ivPosterStore
        internal val smallIcon: CircleImageView = itemView.icon_small
    }

}