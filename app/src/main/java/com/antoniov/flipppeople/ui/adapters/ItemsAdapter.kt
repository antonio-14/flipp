package com.antoniov.flipppeople.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.antoniov.flipppeople.R
import com.antoniov.flipppeople.models.Items
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_item.view.*

class ItemsAdapter(private val listItems: MutableList<Items>, private val listener: ItemsAdapterListener) : RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context), parent)
    }

    fun updateItems(items: List<Items>, notify: Boolean) {
        this.listItems.clear()
        this.listItems.addAll(items)
        if (notify) notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with (listItems[position]){

            val context = holder.image.context
            holder.name.text = name
            val priceSimbol = "${price}$"
            holder.price.text = priceSimbol

            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(20))

            Glide.with(context)
                .load(photo)
                .apply(requestOptions)
                .error(R.drawable.image_placeholder)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.image)
            }
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    inner class ViewHolder internal constructor(
        inflater: LayoutInflater,
        parent: ViewGroup
    ) : RecyclerView.ViewHolder(
        inflater.inflate(R.layout.item_item, parent, false)
    ) {
        internal val name: TextView = itemView.tvName
        internal val price: TextView = itemView.tvPrice
        internal val image: ImageView = itemView.ivPosterItem
    }


    interface ItemsAdapterListener {
        fun removeItem(item: Items)
    }
}