package com.antoniov.flipppeople.ui.list

import com.antoniov.flipppeople.models.Cart
import com.antoniov.flipppeople.models.Items
import com.antoniov.flipppeople.ui.list.interfaces.IUseCaseList

class ListViewModel(private val cart: Cart, private val iUseCaseList: IUseCaseList.View) : IUseCaseList {

   override fun start() {
        loadCart(true)
    }

    override fun loadCart(notify: Boolean) {
       iUseCaseList.showCart(cart.getCartItems(), notify)
    }

    override fun removeItem(item: Items) {
        cart.removeItem(item)
    }

}