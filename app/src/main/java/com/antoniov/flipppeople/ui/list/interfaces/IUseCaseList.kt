package com.antoniov.flipppeople.ui.list.interfaces

import com.antoniov.flipppeople.models.Items

interface IUseCaseList {

    interface View {
         fun showCart(items: List<Items>, notify: Boolean)
    }
    fun start()
     fun loadCart(notify: Boolean)
    fun removeItem(item: Items)
}