package com.antoniov.flipppeople.ui.detailsWalmart

import android.content.Context
import android.util.Log
import com.antoniov.flipppeople.models.Cart
import com.antoniov.flipppeople.models.Items
import com.antoniov.flipppeople.ui.detailsWalmart.interfaces.IItemsRepository
import com.antoniov.flipppeople.utils.vo.Resource
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException

object DetailsWalmartRepository: IItemsRepository {
    override suspend fun getItemsRepo(): Resource<MutableList<Items>> {
        return Resource.Success((getItems() as MutableList<Items>))
    }

    private val TAG = "FoodRepository"

    private lateinit var items: MutableList<Items>

    fun loadItems(context: Context) {
        val gson = Gson()
        val json = loadJSONFromAsset("items.json", context)
        val listType = object : TypeToken<List<Items>>() {}.type
        items = gson.fromJson(json, listType)
        items
            .filter { Cart.isSelect(it) }
            .forEach { it.isSelect = true }
    }

    fun getItems() = items.sortedBy { it.name }

    fun getItemsById(id: Int) = items.firstOrNull { it.id == id }

    private fun loadJSONFromAsset(filename: String, context: Context): String? {
        var json: String? = null
        try {
            val inputStream = context.assets.open(filename)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            json = String(buffer)
        } catch (ex: IOException) {
            Log.e(TAG, "Error reading from $filename", ex)
        }
        return json
    }
}