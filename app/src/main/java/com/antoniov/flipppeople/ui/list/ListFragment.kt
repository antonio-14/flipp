package com.antoniov.flipppeople.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.antoniov.flipppeople.R
import com.antoniov.flipppeople.models.Items
import com.antoniov.flipppeople.ui.adapters.ItemsAdapter
import com.antoniov.flipppeople.ui.injection
import com.antoniov.flipppeople.ui.list.interfaces.IUseCaseList
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment(), IUseCaseList.View, ItemsAdapter.ItemsAdapterListener {

    lateinit var listViewModel: IUseCaseList
    private val adapter = ItemsAdapter(mutableListOf(), this)

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_list, container, false)

        return root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listViewModel = injection.provideCartViewModel(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        listViewModel.start()
    }

    private fun setupRecyclerView() {
        rvListItems.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rvListItems.adapter = adapter
    }

    override fun showCart(items: List<Items>, notify: Boolean) {
        adapter.updateItems(items, notify)
        if (items.isEmpty()) {
            emptyLabel.visibility = View.VISIBLE
            rvListItems.visibility = View.INVISIBLE
        } else {
            emptyLabel.visibility = View.INVISIBLE
            rvListItems.visibility = View.VISIBLE
        }
    }

    override fun removeItem(item: Items) {
        listViewModel.removeItem(item)
    }
}
