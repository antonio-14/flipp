package com.antoniov.flipppeople.ui.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.antoniov.flipppeople.ui.detailsWalmart.interfaces.IUseCaseItems
import com.antoniov.flipppeople.ui.list.interfaces.IUseCaseList

class ListViewModelFactory(private val iUseCaseList: IUseCaseList): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(IUseCaseItems::class.java).newInstance(iUseCaseList)
    }
}