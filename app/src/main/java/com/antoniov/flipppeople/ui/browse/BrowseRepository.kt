package com.antoniov.flipppeople.ui.browse
import com.antoniov.flipppeople.models.Stores
import com.antoniov.flipppeople.ui.browse.interfaces.IStoresRepository
import com.antoniov.flipppeople.utils.vo.Resource

class BrowseRepository: IStoresRepository {

    override suspend fun getStoresRepo(): Resource<MutableList<Stores>> {
        return Resource.Success(setListStores())
    }

     private fun setListStores(): MutableList<Stores> {
        val stores = mutableListOf(Stores("Walmart", "https://g02gadege1516m.files.wordpress.com/2015/11/walmart_logo.jpg?w=959", "22 days left", "https://lh3.googleusercontent.com/tyqhEqe5DP9sTEi0I7XMDmaGhjOtWpQENTSSMZRr95dahMFlYlaGVe3v3EWV-wMmug=s180-rw", 1),
        Stores("Walgreens", "https://www.cuponeando.net/wp-content/uploads/2012/03/walgreens.jpg", "2 week left", "https://www.pngitem.com/pimgs/m/5-50543_logo-walgreens-w-walgreens-logo-hd-png-download.png", 3))
         return stores
    }
}