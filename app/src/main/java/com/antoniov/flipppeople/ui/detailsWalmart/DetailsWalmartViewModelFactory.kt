package com.antoniov.flipppeople.ui.detailsWalmart

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.antoniov.flipppeople.ui.browse.interfaces.IUseCaseStores
import com.antoniov.flipppeople.ui.detailsWalmart.interfaces.IUseCaseItems

class DetailsWalmartViewModelFactory(private val iUseCaseItems: IUseCaseItems): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(IUseCaseItems::class.java).newInstance(iUseCaseItems)
    }
}