package com.antoniov.flipppeople.utils

import android.content.Context
import android.view.View
import android.view.animation.AnimationUtils
import com.antoniov.flipppeople.R
import com.blogspot.atifsoftwares.animatoolib.Animatoo

class AnimationUtils {

    companion object {

        fun doBounceAnimation(mContext: Context, targetView: View) {
            val bounceAnimation = AnimationUtils.loadAnimation(mContext, R.anim.bounce_animation)
            targetView.startAnimation(bounceAnimation)
        }

        fun animationFade(mContext: Context) {
            Animatoo.animateFade(mContext)
        }

    }

}