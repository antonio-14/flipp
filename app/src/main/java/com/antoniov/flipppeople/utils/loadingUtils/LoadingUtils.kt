package com.antoniov.flipppeople.utils.loadingUtils

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.lottie.LottieAnimationView

class LoadingUtils {

    companion object {

        fun showLottie(animation: LottieAnimationView, layoutLotie: ConstraintLayout) {
            layoutLotie.visibility = View.VISIBLE
            animation.playAnimation()
        }

        fun hideLottie(animation: LottieAnimationView, layoutLotie: ConstraintLayout) {
            layoutLotie.visibility = View.GONE
            animation.pauseAnimation()
        }
    }
}