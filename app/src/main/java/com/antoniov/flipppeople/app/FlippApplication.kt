package com.antoniov.flipppeople.app

import android.app.Application
import android.content.Context
import com.antoniov.flipppeople.ui.detailsWalmart.DetailsWalmartRepository

class FlippApplication : Application() {

    companion object {
        private lateinit var instance: FlippApplication

        fun getAppContext(): Context = instance.applicationContext
    }

    override fun onCreate() {
        instance = this
        super.onCreate()

        // Initialize FoodRepository
        DetailsWalmartRepository.loadItems(this)
    }
}