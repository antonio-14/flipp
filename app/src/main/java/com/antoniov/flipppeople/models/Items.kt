package com.antoniov.flipppeople.models

data class Items(
    val id: Int,
    val name: String,
    val price: String,
    val photo: String,
    var isSelect: Boolean = false
)