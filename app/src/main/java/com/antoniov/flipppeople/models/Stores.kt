package com.antoniov.flipppeople.models

data class Stores(
    val title: String,
    val photo: String,
    val description: String,
    val icon: String,
    val id: Int
)