package com.antoniov.flipppeople.models

import androidx.preference.PreferenceManager
import com.antoniov.flipppeople.app.FlippApplication
import com.antoniov.flipppeople.ui.detailsWalmart.DetailsWalmartRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

object Cart {

    private val KEY_CART = "KEY_CART"
    private val gson = Gson()

    private var itemIds: MutableList<Int>? = null

    fun isSelect(item: Items): Boolean {
        return getCartItemIds()?.contains(item.id) == true
    }

    fun addItem(item: Items) {
        val itemIds = getCartItemIds()
        itemIds?.let {
            item.isSelect = true
            itemIds.add(item.id)
            saveCart(KEY_CART, itemIds)
        }
    }

    fun removeItem(item: Items) {
        val itemIds = getCartItemIds()
        itemIds?.let {
            item.isSelect = false
            itemIds.remove(item.id)
            saveCart(KEY_CART, itemIds)
        }
    }

    private fun saveCart(key: String, list: List<Int>) {
        val json = gson.toJson(list)
        sharedPrefs().edit().putString(key, json).apply()
    }

    private fun getCartItemIds(): MutableList<Int>? {
        if (itemIds == null) {
            val json = sharedPrefs().getString(KEY_CART, "")
            val type = object : TypeToken<MutableList<Int>>() {}.type
            itemIds = gson.fromJson<MutableList<Int>>(json, type) ?: return mutableListOf()
        }
        return itemIds
    }

    fun getCartItems(): MutableList<Items> {
        val itemIds = getCartItemIds()
        val items = mutableListOf<Items>()
        itemIds?.let {
            itemIds.forEach {
                val item = DetailsWalmartRepository.getItemsById(it)
                item?.let {
                    items.add(item)
                }
            }
        }
        return items
    }

    fun cartSize(): Int {
        val itemIds = getCartItemIds()
        itemIds?.let {
            return itemIds.size
        }
        return 0
    }

    private fun sharedPrefs() = PreferenceManager.getDefaultSharedPreferences(FlippApplication.getAppContext())
}